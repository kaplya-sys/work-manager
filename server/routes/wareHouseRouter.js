const express = require('express')
const router = express.Router()
const passport = require('passport')
const controller = require('../controllers/wareHouseController')

router.post('/add-product', passport.authenticate('jwt', {session: false}), controller.addProduct)
router.delete('/delete-product', passport.authenticate('jwt', {session: false}), controller.deleteProduct)
router.patch('/product-data', passport.authenticate('jwt', {session: false}), controller.addProductCountOrSN)

module.exports = router