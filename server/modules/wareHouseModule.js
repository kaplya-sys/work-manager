module.exports = (sequelize, DataTypes) => {
    return WareHouse = sequelize.define('warehouse', {
        productId: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        company: {
            type: DataTypes.STRING,
        },
        category: {
            type: DataTypes.STRING,
        },
        productName: {
            type: DataTypes.STRING,
        },
        productCountOrSN: {
            type: DataTypes.ARRAY(DataTypes.STRING),
        },
        unitMeters: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }
    }, {timestamps: false})
}