module.exports = (sequelize, DataTypes) => {
    return UserBackpack = sequelize.define('userBackpack', {
        productId: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        category: {
            type: DataTypes.STRING,
        },
        productName: {
            type: DataTypes.STRING,
        },
        productCountOrSN: {
            type: DataTypes.ARRAY(DataTypes.STRING)
        },
        unitMeters: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }
    })
}