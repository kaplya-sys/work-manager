module.exports = (sequelize, DataTypes) => {
    return AppointProduct = sequelize.define('appointProduct', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        personalAccount: {
            type: DataTypes.INTEGER
        },
        fullName: {
            type: DataTypes.STRING
        },
        billingAddress: {
            type: DataTypes.STRING
        },
        productName: {
            type: DataTypes.STRING
        },
        productCountOrSN: {
            type: DataTypes.ARRAY(DataTypes.STRING)
        }
    })
}