const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const db = require('../db')
const User = db.User
const secret = require('../config/jwtConfig')

module.exports.login = async(req, res) => {
    try {
        const userLogin = req.body.login
        const userPassword = req.body.password

        const result = await User.findOne({
            where: {login: userLogin},
            raw: true
        })

        if (result) {
            const {id, password, firstName, lastName, avatar, isAdmin} = result
            const checkedPassword = bcrypt.compareSync(userPassword ,password)

            if (checkedPassword) {
                const token = jwt.sign({
                    _id: id
                }, secret.jwt, {expiresIn: '1d'})

                return res.status(200).json({
                    userToken: `Bearer ${token}`,
                    user: {
                        id,
                        firstName,
                        lastName,
                        avatar,
                        isAdmin,
                    }
                })
            } else {
                return res.status(401).json({
                    message: 'Введён не верный пароль.'
                })
            }
        } else {
            return res.status(404).json({
                message: 'Пользователь с таим логином не найден.'
            })
        }
    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m', err)
        
        return res.status(500).json({
            message: 'Извините, что-то пошло не так.'
        })
    }
}

module.exports.create = async(req, res) => {
    try {
        const {
            firstName,
            lastName,
            patronymic,
            company,
            login,
            password,
            isAdmin
        } = req.body

        const result = await User.findOne({
            where: {login: login},
            raw: true
        })

        if (result) {
            return res.status(409).json({
                message: 'Пользователь с данным логином уже зарегистрирован.'
            })
        } else {
            const salt = bcrypt.genSaltSync(10)
            const hashedPassword = bcrypt.hashSync(password, salt)

            await User.create({
                login,
                password: hashedPassword,
                firstName,
                lastName,
                patronymic,
                company,
                isAdmin
            })

            return res.status(201).json({
                message: 'Пользователь успешно создан.'
            })
        }
    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m', err)

        return res.status(500).json({
            message: 'Извините, что-то пошло не так.'
        })
    }
}