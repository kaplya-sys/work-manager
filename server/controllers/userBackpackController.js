const {Op, fn, col} = require('sequelize')
const db = require('../db')
const AppointProduct = db.AppointProduct
const WareHouse = db.WareHouse

module.exports.appointEquipment = async(req, res) => {
    try {
        const {productData} = req.body

        await AppointProduct.create(productData)

        return res.status(200).json({
            message: 'The product has been successfully decommissioned.'
        })
    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m', err)
        
        return res.status(500).json({
            message: 'Sorry, something went wrong'
        })
    }
}

module.exports.revertEquipment = async(req, res) => {
    try {
        const {productId, userId} = req.body

        const result = await AppointProduct.findOne(
            {
                where: {
                    [Op.and]: [
                        userId,
                        productId
                    ]
                },
                raw: true
            }
        )
        await AppointProduct.destroy(
            {
                where: {
                    [Op.and]: [
                        userId,
                        productId
                    ]
                }
            }
        )

        if (result.unitMeters) {
            await WareHouse.update(
                {
                    productCountOrSN: result
                },
                {
                    where: {
                        productId
                    }
                }
            )
    
            return res.status(201).json({
                message: 'Changes made successfully'
            })
        }
        await WareHouse.update(
            {
                productCountOrSN: fn('array_cat', col('productCountOrSN'), result)
            },
            {
                where: {
                    productId
                }
            }
        )

        return res.status(200).json({
            message: 'The product has been successfully revert.'
        })
    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m', err)
        
        return res.status(500).json({
            message: 'Sorry, something went wrong'
        })
    }
}

module.exports.reassignEquipment = async(req, res) => {
    try {
        const {userId, productId} = req.body

        const result = await AppointProduct.findOne(
            {
                where: {
                    [Op.and]: [
                        userId,
                        productId
                    ]
                },
                raw: true
            }
        )
        await AppointProduct.destroy(
            {
                where: {
                    [Op.and]: [
                        userId,
                        productId
                    ]
                }
            }
        )   

    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m', err)
        
        return res.status(500).json({
            message: 'Sorry, something went wrong'
        })
    }
}