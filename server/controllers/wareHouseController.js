const {Op, fn, col} = require('sequelize')
const db = require('../db')
const WareHouse = db.WareHouse
const UserBackpack = db.UserBackPack
 
module.exports.addProduct = async(req, res) => {
    try {
        const {product} = req.body
        const {productName, company} = product

        const result = await WareHouse.findOne({
            where: {
                [Op.and]: [
                    {productName},
                    {company}
                ]
            },
            raw: true
        })

        if (result) {
            return res.status(409).json({
                message: 'Данный материал уже числится на складе.'
            })
        } else {
            await WareHouse.create(product)
        }

        return res.status(201).json({
            message: 'Материал успешно добавлен.'
        })
    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m', err)

        return res.status(500).json({
            message: 'Извините, что-то пошло не так.'
        })
    }
}

module.exports.getProduct = async(req, res) => {
    try {
        const {productId} = req.body

        const product = await WareHouse.findOne(
            {
                where: {productId},
                raw: true
            }
        )

        return res.status(200).json({product})
    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m', err)
        
        return res.status(500).json({
            message: 'Извините, что-то пошло не так.'
        })
    }
}

module.exports.getAllProduct = async(req, res) => {
    try {
        const products = await WareHouse.findAll({raw: true}) 

        return res.status(200).json({products})
    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m', err)
        
        return res.status(500).json({
            message: 'Извините, что-то пошло не так.'
        })
    }
}

module.exports.deleteProduct = async(req, res) => {
    try {
        const {productId} = req.body

        await WareHouse.destroy({where: {productId}})

        return res.status(200).json({
            message: 'Материал успешно удален.'
        })
    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m', err)

        res.status(500).json({
            message: 'Извините, что-то пошло не так.'
        })
    }
}

module.exports.addProductCountOrSN = async(req, res) => {
    try {
        const {productData, productId, unitMeters} = req.body.product

        if (unitMeters) {
            await WareHouse.update(
                {
                    productCountOrSN: productData
                },
                {
                    where: {
                        productId
                    }
                }
            )
    
            return res.status(201).json({
                message: 'Количество материала успешно обновлено.'
            })
        } else {
            await WareHouse.update(
                {
                    productCountOrSN: fn('array_cat', col('productCountOrSN'), productData)
                },
                {
                    where: {
                        productId
                    }
                }
            )
    
            return res.status(201).json({
                message: 'Серийные номера успешно обновлены.'
            })
        }
    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m', err)

        return res.status(500).json({
            message: 'Извините, что-то пошло не так.'
        })
    }
}

module.exports.assignToUser = async(req, res) => {
    try {
        const {assignData} = req.body
        const {productId, userId, data, unitMeters} = assignData

        const result = await WareHouse.findOne(
            {
                where: {
                    [Op.and]: [
                        {productId},
                        {productCountOrSN: data}
                    ]
                },
                raw: true
            }
        )
        if (unitMeters) {
            await WareHouse.update(
                {
                    productCountOrSN: fn('array_replace', col('productCountOrSN'), data)
                },
                {
                    where: {
                        [Op.and]: [
                            {productId},
                            {productCountOrSN: data}
                        ]
                    }
                }
            )
        } else {

        }


        await WareHouse.destroy(
            {
                where: {
                    productId
                }
            }
        )

        if (result.unitMeters) {
            await UserBackpack.update(
                {
                    productCountOrSN: result
                },
                {
                    where: {
                        [Op.and]: [
                            userId,
                            productId
                        ]
                    }
                }
            )
    
            return res.status(201).json({
                message: 'The product was successfully assigned.'
            })
        } else {
            await UserBackpack.update(
                {
                    productCountOrSN: fn('array_cat', col('productCountOrSN'), result)
                },
                {
                    where: {
                        [Op.and]: [
                            userId,
                            productId
                        ]
                    },
                }
            )
    
            return res.status(200).json({
                message: 'The product was successfully assigned.'
            })
        }
    } catch(err) {
        
    }
}