const Sequelize = require('sequelize')
require('dotenv').config()

const {DB_NAME, DB_HOST, DB_DIALECT, DB_USER, DB_PASSWORD} = process.env
 
const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD, {
    host: DB_HOST,
    dialect: DB_DIALECT,
    operatorsAliases: 0,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
})

;(
    async () => {
        try {
            await sequelize.authenticate()
            console.log('\x1b[32m%s\x1b[0m', 'Connection has been established successfully.')
        } catch(err){
            console.log('\x1b[31m%s\x1b[0m', 'Unable to connect to the database:', err)
        }
    }
)()

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.User = require('../modules/userModule')(sequelize, Sequelize)
db.WareHouse = require('../modules/wareHouseModule')(sequelize, Sequelize)
db.UserBackPack = require('../modules/userBackpackModule')(sequelize, Sequelize)
db.AppointProduct = require('../modules/appointProductModule')(sequelize, Sequelize)

db.User.hasMany(db.UserBackPack)
db.UserBackPack.belongsTo(db.User)
db.WareHouse.hasMany(db.AppointProduct)
db.AppointProduct.belongsTo(db.User)


module.exports = db