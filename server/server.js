const express = require('express')
const db = require('./db')
const bodyParser = require('body-parser')
const passport = require('passport')

require('dotenv').config()

const authRouter = require('./routes/authRouter')
const wareHouseRouter = require('./routes/wareHouseRouter')

const PORT = process.env.SERVER_PORT || 5000

;(
    async() => {
        try {
            await db.sequelize.sync()
            console.log('\x1b[32m%s\x1b[0m', 'All models were synchronized successfully.')
        } catch(err) {
            console.log('\x1b[31m%s\x1b[0m', 'Unable to synchronized to the models:', err)
        }
    }
)()

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.use(passport.initialize())
require('./middleware/passport')(passport)

app.use('/api/auth', authRouter)
app.use('/api/wareHouse', wareHouseRouter)

app.listen(PORT, () => {
    console.log('\x1b[34m%s\x1b[0m', `Server started.. on port: ${PORT}`)
})