const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const db = require('../db')
const secret = require('../config/jwtConfig')

const User = db.User
const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: secret.jwt
}

module.exports = passport => {
    passport.use(
        new JwtStrategy(options, async(jwt_payload, done) => {
            try {
                const user = await User.findOne({
                    where: {id: jwt_payload._id},
                    raw: true
                })
                
                if (user) {
                    done(null, user)
                } else {
                    done(null, false)
                }
            } catch (err) {
                console.log('\x1b[31m%s\x1b[0m', err)
            }
        })
    )
}