import React, { useEffect, useState, useCallback, FC } from "react"
import * as SplashScreen from 'expo-splash-screen'
import * as Font from "expo-font"
import { Alert } from "react-native"
import Toast from 'react-native-simple-toast'
import { RootNavigation } from "./src/navigate/RootNavigation"
import { useAppDispatch, useAppSelector } from "./src/store/app/hook"
import { Loader } from "./src/components/Loader"
import { getDataFromStorage } from "./src/helpers/StorageHelper"
import { checkAuth } from "./src/store/reducers/authSlice"

export const AppLoading: FC = () => {
    const [isReady, setIsReady] = useState<boolean>(false)
    const { error, isLoading } = useAppSelector(state => state.appSlice)
    const dispatch = useAppDispatch()

    useEffect(() => {
        const prepare = async () => {
            try {
                await SplashScreen.preventAutoHideAsync()
                await Font.loadAsync({
                    'roboto-bold-font': require('./assets/fonts/Roboto-Bold.ttf'),
                    'roboto-italic-font': require('./assets/fonts/Roboto-Italic.ttf'),
                    'roboto-regular-font': require('./assets/fonts/Roboto-Regular.ttf'),
                })
            } catch (e) {
                console.warn(e)
            } finally {
                setIsReady(true)
            }
        }

        prepare()
    }, [])

    useEffect(() => {
        error && Alert.alert(error)
    }, [error])

    useEffect(() => {
        const getToken = async() => {
            const token = await getDataFromStorage()
            
            token?.user && dispatch(checkAuth(token.user))
        }
        getToken()
    }, [])

    const onLayoutRootView = useCallback(async () => {
        if (isReady) {
            await SplashScreen.hideAsync()
        }
    }, [isReady])

    onLayoutRootView()

    if (!isReady) {
        return null
    }

    if (isLoading) {
        return <Loader />
    }

    return <RootNavigation />
}