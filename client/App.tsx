import 'react-native-gesture-handler'
import React, { FC } from "react"
import { Provider } from 'react-redux'
import { store } from './src/store/store'
import { AppLoading } from './AppLoading'

const App: FC = () => {
	return (
		<Provider store={store}>
			<AppLoading />
		</Provider>
	)
}

export default App