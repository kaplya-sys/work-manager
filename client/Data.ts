export const DATA = [
    {
        id: '1',
        title: 'Eltex 1124',
        data: ['ES20005590', 'ES20005591', 'ES20005592', 'ES20005590', 'ES20005591', 'ES20005592', 'ES20005590', 'ES20005591', 'ES20005592', 'ES20005590', 'ES20005591', 'ES20005592']
    },
    {
        id: '2',
        title: 'UTP 5e',
        data: ['305']
    },
    {
        id: '3',
        title: 'OR 8602b',
        data: [
            'OR414935',
            'OR414936',
            'OR414944', 
            'OR414945',
            'OR414946',
            'OR414950',
            'OR414935',
            'OR414936', 
            'OR414944',
            'OR414945',
            'OR414946',
            'OR414950' , 
            'OR414935', 
               'OR414936', 
               'OR414944', 
               'OR414945', 
               'OR414946', 
               'OR414950', 
               'OR414935', 'OR414936', 'OR414944', 'OR414945', 'OR414946', 'OR414950' , 'OR414935', 'OR414936', 'OR414944', 'OR414945', 'OR414946', 'OR414950']
    }
]

export const USERS = [
    {
        id: '1',
        login: 'ivan',
        password: '111111',
        firstName: 'Иван',
        lastName: 'Иванов',
        patronymic: '',
        company: 'юг',
        avatar: '',
        isAdmin: false,
        equipments: []
    },
    {
        id: '2',
        login: 'dimon',
        password: '111111',
        firstName: 'Дима',
        lastName: 'Воробьев',
        patronymic: '',
        company: 'север',
        avatar: '',
        isAdmin: false,
        equipments: []
    },
    {
        id: '3',
        login: 'sanchez',
        password: '111111',
        firstName: 'Саша',
        lastName: 'Некрасов',
        patronymic: '',
        company: 'юг',
        avatar: '',
        isAdmin: false,
        equipments: []
    },
]

export type DataType = {
    id: string
    title: string
    data: string[]
}

export type UserType = {
	id: string
	login: string
	password: string
	firstName: string
	lastName: string
	patronymic: string
	company: string
	avatar: string
	isAdmin: boolean
	equipments: any[]
}