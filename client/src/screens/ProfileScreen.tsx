import { Button, Image, StyleSheet, Text, View } from 'react-native'
import React, { FC } from 'react'
import {USERS, UserType} from '../../Data'

export const ProfileScreen: FC = () => {
  const user = USERS.find(item => item.id === '1') as UserType
  
  return (
    <View>
      <Image
      source={user.avatar? require('../../assets/default-avatar.png'): require('../../assets/default-avatar.png')}
      style={{ width: 80, height: 80 }}
      />
      <Text>{`${user.firstName} ${user.lastName}`}</Text>
      <Button title='изменить пароль'/>
      <Button title='изменить логин'/>
      <Button title='изменить аватар' onPress={() => console.log()}/>
    </View>
  )
}

const styles = StyleSheet.create({})