import { FlatList, Pressable, StyleSheet, Text, View } from 'react-native'
import React, { FC, useState } from 'react'
import { USERS, UserType } from '../../Data'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { EquipmentStackParamList } from '../navigate/types'
import { CustomButton } from '../components/CustomButton'

type Props = NativeStackScreenProps<EquipmentStackParamList, 'ReassignEquipment'>

export const ReassignEquipmentScreen: FC<Props> = ({ route }) => {
	const [title, setTitle] = useState<string>('Сотрудники')
	const [userId, setUserId] = useState<string>('')
	const [show, setShow] = useState<boolean>(false)
	const { equipment, serial } = route.params

	const reassignData = {
		userId,
	}

	const renderItemHandler = ({ item }: { item: UserType }) => {
		return (
			<>
				{show && <Pressable onPress={() => {
					setTitle(`${item.firstName} ${item.lastName}`)
					setUserId(item.id)
					setShow(!show)
				}}>
					<Text>
						{`${item.firstName} ${item.lastName}`}
					</Text>
				</Pressable>}
			</>
		)
	}

	return (
		<View>
			<Text>
				{`Оборудование: ${equipment} S/N: ${serial}`}
			</Text>
			<Pressable onPress={() => setShow(!show)}
			>
				<Text style={styles.titleStyle}>{title}</Text>
			</Pressable>
			<FlatList
				data={USERS}
				renderItem={renderItemHandler}
				keyExtractor={item => item.id}
			/>
			<CustomButton
				text='Переназначить'
				type='filled'
				onPress={() => console.log("sending...")}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	titleStyle: {
		borderWidth: 1,
		padding: 1,
		width: 110,
		textAlign: 'center'
	}
})