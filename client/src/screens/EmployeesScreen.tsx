import { FlatList, StyleSheet, SafeAreaView, Text, View, Button } from 'react-native'
import React, { FC } from 'react'
import { USERS } from '../../Data'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { EmployeesStackParamList } from '../navigate/types'

type Props = NativeStackScreenProps<EmployeesStackParamList, 'Employees'>

export const EmployeesScreen: FC<Props> = ({navigation}) => {
	return (
		<SafeAreaView>
			<View style={styles.employeesContainer}>
				<FlatList
					data={USERS}
					keyExtractor={user => user.id}
					renderItem={({ item }) => (
						<View style={styles.employeeFlatList}>
							<Text>{`${item.firstName} ${item.lastName}`}</Text>
						</View>
					)}
				/>
				<Button title='Добавить' onPress={() => navigation.navigate('AddEmployees')} />
			</View>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	employeeFlatList: {
		marginBottom: 5
	},
	employeesContainer: {
		padding: 20
	}
})