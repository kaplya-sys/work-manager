import { Button, StyleSheet, View } from 'react-native'
import React, { FC, useState } from 'react'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { CustomTextInput } from '../components/CustomTextInput'
import { EquipmentStackParamList } from '../navigate/types'

type Props = NativeStackScreenProps<EquipmentStackParamList, 'AppointEquipment'>

type EquipmentDataType = {
	personalAccount: string
	fullName: string
	billingAddress: string
	equipment: string
	serialNumber: string
}

export const AppointEquipmentScreen: FC<Props> = ({route}) => {
	const {equipment, serial} = route.params
	const [equipmentData, setEquipmentData] = useState<EquipmentDataType>({
		personalAccount: '',
		fullName: '',
		billingAddress: '',
		equipment: equipment,
		serialNumber: serial
	})
	const {personalAccount, fullName, billingAddress} = equipmentData

	return (
		<View style={styles.appointEquipmentContainer}>
			<CustomTextInput
				value={personalAccount}
				placeholder='Лицевой счёт'
				keyboardType='number-pad'
				onChangeText={(text: string) => setEquipmentData(prev => ({...prev, personalAccount: text}))}
			/>
			<CustomTextInput
				value={fullName}
				placeholder='Ф.И.О'
				onChangeText={(text: string) => setEquipmentData(prev => ({...prev, fullName: text}))}
			/>
			<CustomTextInput
				value={billingAddress}
				placeholder='Адрес списания'
				onChangeText={(text: string) => setEquipmentData(prev => ({...prev, billingAddress: text}))}
			/>
			<Button
				title='Отправить'
				onPress={() => {
					setEquipmentData(prev => ({
						...prev,
						personalAccount: '',
						fullName: '',
						billingAddress: '',
					}))
					console.log('Pending subscriberData ...')}
				}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	appointEquipmentContainer: {
		padding: 20
	}
})