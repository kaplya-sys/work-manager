import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { FC } from 'react'
import { AppBarCodeScanner } from '../components/AppBarCodeScanner'
import { AppImagePicker } from '../components/AppImagePicker'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { CustomTextInput } from '../components/CustomTextInput'

export const AddEquipmentScreen: FC = () => {
	return (
		<View style={styles.container}>
			<TouchableOpacity
				style={styles.barCodeScannerButton}
				onPress={() => console.log('1')}
			>
				<MaterialCommunityIcons name="barcode-scan" size={24} color="black" />
			</TouchableOpacity>
			<CustomTextInput />
			<CustomTextInput />
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		flexWrap: "wrap"
	},
	barCodeScannerButton: {
	}
})