import { Alert, Button, Keyboard, StyleSheet, View, Switch, Text, TouchableWithoutFeedback, ScrollView } from 'react-native'
import React, { FC, useState } from 'react'
import { CustomTextInput } from '../components/CustomTextInput'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { EmployeesStackParamList } from '../navigate/types'
import { useAppDispatch } from '../store/app/hook'
import { addEmployee } from '../store/asyncThunks/addEmployee'

type Props = NativeStackScreenProps<EmployeesStackParamList, 'AddEmployees'>

type UserType = {
	firstName: string
	lastName: string
	patronymic: string
	company: string
	login: string
	password: string
	isAdmin: boolean
}

export const AddEmployeesScreen: FC<Props> = ({navigation}) => {
	const [rePassword, setRePassword] = useState<string>('')
	const [userData, setUserData] = useState<UserType>({
		firstName: '',
		lastName: '',
		patronymic: '',
		company: '',
		login: '',
		password: '',
		isAdmin: false
	})
	const dispatch = useAppDispatch()
	const {firstName, lastName, patronymic, company, login, password, isAdmin} = userData

	const handlerSubmit = (): void => {
		if (password !== rePassword) {
			return Alert.alert("Пароли не совпадают!")
		}
		dispatch(addEmployee(userData))
		setUserData(prev => ({
			...prev,
			firstName: '',
			lastName: '',
			patronymic: '',
			company: '',
			login: '',
			password: '',
		}))
		setRePassword('')
		navigation.navigate('Employees')
	}

	return (
		<ScrollView>
			<TouchableWithoutFeedback onPress={() => Keyboard.dismiss}>
				<View style={styles.employeesContainer}>
					<CustomTextInput
						value={firstName}
						placeholder='Имя'
						style={styles.input}
						onChangeText={(text: string) => setUserData(prev => ({ ...prev, firstName: text }))}
					/>
					<CustomTextInput
						value={lastName}
						placeholder='Фамилия'
						style={styles.input}
						onChangeText={(text: string) => setUserData(prev => ({ ...prev, lastName: text }))}
					/>
					<CustomTextInput
						value={patronymic}
						placeholder='Отчество'
						style={styles.input}
						onChangeText={(text: string) => setUserData(prev => ({ ...prev, patronymic: text }))}
					/>
					<CustomTextInput
						value={company}
						placeholder='Компания'
						style={styles.input}
						onChangeText={(text: string) => setUserData(prev => ({ ...prev, company: text }))}
					/>
					<CustomTextInput
						value={login}
						placeholder="Логин"
						style={styles.input}
						onChangeText={(text: string) => setUserData(prev => ({ ...prev, login: text }))}
					/>
					<CustomTextInput
						value={password}
						placeholder="Пароль"
						secureTextEntry
						style={styles.input}
						onChangeText={(text: string) => setUserData(prev => ({ ...prev, password: text }))}
					/>
					<CustomTextInput
						value={rePassword}
						placeholder="Повторите пароль"
						secureTextEntry
						style={styles.input}
						onChangeText={setRePassword}
					/>
					<View style={styles.employeesSwitch}>
						<Text style={styles.employeesText}>Администратор:</Text>
						<Switch
							trackColor={{ false: "#767577", true: "#81b0ff" }}
							thumbColor='#778899'
							ios_backgroundColor='#3e3e3e'
							value={isAdmin}
							onValueChange={() => setUserData(prev => ({ ...prev, isAdmin: !isAdmin }))}
						/>
					</View>
					<Button
						title='Отправить'
						onPress={handlerSubmit}
					/>
				</View>
			</TouchableWithoutFeedback>
		</ScrollView>
	)
}

const styles = StyleSheet.create({
	employeesContainer: {
		padding: 20,
	},
	employeesSwitch: {
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	employeesText: {
		marginTop: 15,
		marginRight: 0,
	},
	input: {}
})