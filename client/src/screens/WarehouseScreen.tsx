import { StyleSheet, View, Button } from 'react-native'
import React, { FC } from 'react'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { WarehouseStackParamList } from '../navigate/types'

type Props = NativeStackScreenProps<WarehouseStackParamList, 'Warehouse'>

export const WarehouseScreen: FC<Props> = ({navigation}) => {
  return (
    <View>
      <Button title='Добавить' onPress={() => navigation.navigate('AddWarehouse')}/>
    </View>
  )
}

const styles = StyleSheet.create({})