import { StyleSheet, View } from 'react-native'
import React, { FC, useState } from 'react'
import { CustomTextInput } from '../components/CustomTextInput'
import { CustomButton } from '../components/CustomButton'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { AuthStackParamList } from '../navigate/types'
import { useAppDispatch } from '../store/app/hook'
import { loginIn } from '../store/asyncThunks/loginIn'

type Props = NativeStackScreenProps<AuthStackParamList, 'SingUp'>

type AuthDataType = {
	login: string
	password: string
}

export const AuthScreen: FC<Props> = ({navigation}) => {
	const [authData, setAuthData] = useState<AuthDataType>({
		login: '',
		password: ''
	})
	const dispatch = useAppDispatch()
	const { login, password } = authData


	return (
		<View style={styles.container}>
			<CustomTextInput
				value={login}
				placeholder='Логин'
				onChangeText={(text: string) => setAuthData(prev => ({ ...prev, login: text }))}
			/>
			<CustomTextInput
				value={password}
				placeholder='Пароль'
				secureTextEntry
				onChangeText={(text: string) => setAuthData(prev => ({ ...prev, password: text }))}
			/>
			<CustomButton
				text='Авторизоваться'
				type='filled'
				onPress={() => {
					dispatch(loginIn(authData))
					setAuthData(prev => ({...prev, login: '', password: ''}))
				}}
			/>
			<CustomButton
				text='Забыли пароль?'
				type='lowercase'
				onPress={() => {
					navigation.navigate('PasswordRecovery')
				}}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		padding: 20
	},
})