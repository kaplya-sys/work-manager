import { Pressable, SafeAreaView, SectionList, StyleSheet, Text, View } from 'react-native'
import React, { FC, useState } from 'react'
import { DATA, DataType } from '../../Data'
import { EquipmentModalWindow } from '../components/EquipmentModalWindow'

type EquipmentDataType = {
    serial: string
    equipment: string
}

export const EquipmentScreen: FC = () => {
    const [titleId, setTitleId] = useState<string>('')
    const [visible, setVisible] = useState<boolean>(false)
    const [show, setShow] = useState<boolean>(false)
    const [equipmentData, setEquipmentData] = useState<EquipmentDataType>({
        serial: '',
        equipment: ''
    })

    const renderItemHandler = ({ item, section }: { item: string, section: DataType }) => {

        return (
            <>
                {section.id === titleId && show &&
                    <Pressable
                        onLongPress={() => {
                            setEquipmentData(prev => ({
                                ...prev,
                                serial: item,
                                equipment: section.title
                            }))
                            setVisible(true)
                        }}
                    >
                        <Text
                            style={styles.dataText}
                        >
                            {item}
                        </Text>
                    </Pressable>}
            </>
        )
    }

    const renderSectionHandler = ({ section }: { section: DataType }) => {

        return (
            <Pressable
                onPress={() => {
                    setTitleId(section.id)
                    setShow(!show)
                }}
            >
                <Text
                    style={styles.titleText}
                >
                    {section.title}
                </Text>
            </Pressable>
        )
    }

    return (
        <SafeAreaView>
            <EquipmentModalWindow
                equipmentData={equipmentData}
                hideModal={() => setVisible(false)}
                visible={visible}
            />
            <View style={styles.wrapperStyle}>
                <SectionList
                    sections={DATA}
                    keyExtractor={(item, index) => item + index}
                    renderItem={renderItemHandler}
                    renderSectionHeader={renderSectionHandler}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    wrapperStyle: {
        padding: 20
    },
    dataText: {
        backgroundColor: "#808080",
        padding: 5,
        marginVertical: 5
    },
    titleText: {
        fontFamily: 'roboto-bold-font',
        marginBottom: 10
    }
})