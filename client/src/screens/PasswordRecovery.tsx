import { StyleSheet, View } from 'react-native'
import React, { FC, useState } from 'react'
import { CustomTextInput } from '../components/CustomTextInput'
import { CustomButton } from '../components/CustomButton'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { AuthStackParamList } from '../navigate/types'

type Props = NativeStackScreenProps<AuthStackParamList, 'PasswordRecovery'>

export const PasswordRecovery: FC<Props> = ({navigation}) => {
	const [login, setLogin] = useState<string>('')

	const resetPasswordHandler = () => {
	}

	return (
		<View>
			<CustomTextInput
				value={login}
				placeholder='Логин'
				onChangeText={(text: string) => setLogin(text)}
			/>
			<CustomButton
				text='Сбросить'
				type='filled'
				onPress={() => {
					console.log()
				}}
			/>
		</View>
	)
}

const styles = StyleSheet.create({})