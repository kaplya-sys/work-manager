import { createAsyncThunk } from "@reduxjs/toolkit"
import { AxiosError } from "axios"
import { saveDataToStorage } from "../../helpers/StorageHelper"
import { authorization } from "../../services/authService"
import { AuthDataType, AuthUserResponseDataType } from "../../types"

export const loginIn = createAsyncThunk
    <AuthUserResponseDataType,
        AuthDataType,
        {
            rejectValue: string
        }
    >(
        'authSlice/loginIn',
        async (payload, { rejectWithValue }) => {
            try {
                const response = await authorization(payload)

                const {user, userToken} = response.data as AuthUserResponseDataType

                await saveDataToStorage(userToken, JSON.stringify(user))

                return response.data
            } catch (error) {
                const err = error as AxiosError<{message: string}>

                if (err.response) {
                    return rejectWithValue(err.response.data.message)
                } else if (err.request) {
                    return rejectWithValue(err.request)
                } else {
                    return rejectWithValue(err.message)
                }
            }
        }
    )