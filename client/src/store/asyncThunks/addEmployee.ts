import { registration } from './../../services/authService'
import { createAsyncThunk } from '@reduxjs/toolkit'
import { CreateUserDataType } from '../../types'
import { AxiosError } from 'axios'

export const addEmployee = createAsyncThunk<
    undefined,
    CreateUserDataType,
    {
        rejectValue: string
    }>(
        'authSlice/addEmployee',
        async (payload, { rejectWithValue }) => {
            try {
                await registration(payload)
            } catch (error) {
                const err = error as AxiosError

                if (err.response) {
                    console.log(2222, err.response)
                    return rejectWithValue(err.response.data as any)
                } else if (err.request) {
                    return rejectWithValue(err.request)
                } else {
                    return rejectWithValue(err.message)
                }
            }
        }
    )