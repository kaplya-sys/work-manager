export interface InitialStateType {
    isLoading: boolean
    isDisabled: boolean
    error: string | undefined | null
}