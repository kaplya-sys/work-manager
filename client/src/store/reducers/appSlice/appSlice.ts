import { createSlice, isFulfilled, isPending, isRejected } from '@reduxjs/toolkit'
import { loginIn } from '../../asyncThunks/loginIn'
import { InitialStateType } from './types'

const initialState: InitialStateType = {
    isLoading: false,
    isDisabled: false,
    error: null
}

export const appSlice = createSlice({
    name: 'appSlice',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder
            .addMatcher(isPending(
                loginIn,
            ),
                state => {
                    state.error = null
                    state.isLoading = true
                    state.isDisabled = true
                })
            .addMatcher(isFulfilled(
                loginIn,
            ),
                state => {
                    state.isLoading = false
                    state.isDisabled = false
                })
            .addMatcher(isRejected(
                loginIn,
            ),
                (state, action) => {
                    state.isLoading = false
                    state.isDisabled = false
                    state.error = action.payload
                })
    }
})