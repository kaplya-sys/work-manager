import { appSlice } from './appSlice/appSlice'
import { combineReducers } from "@reduxjs/toolkit"
import { authSlice } from "./authSlice"

export const rootReducer = combineReducers({
    authState: authSlice.reducer,
    appSlice: appSlice.reducer
})