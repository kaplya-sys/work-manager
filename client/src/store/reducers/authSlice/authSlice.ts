import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { UserDataType } from "../../../types"
import { loginIn } from "../../asyncThunks/loginIn"
import { InitialStateType } from "./types"

const initialState: InitialStateType = {
    loggingIn: false,
    user: {}
}

export const authSlice = createSlice({
    name: 'authSlice',
    initialState,
    reducers: {
        logout: (state: InitialStateType) => {
            state.loggingIn = false
            state.user = {}
        },
        checkAuth: (state: InitialStateType, action: PayloadAction<UserDataType>) => {
            state.loggingIn = true
            state.user = action.payload
        }
    },
    extraReducers: builder => {
        builder
            .addCase(loginIn.fulfilled, (state, action) => {
                state.loggingIn = true
                state.user = action.payload.user
            })
    }
})

export const {logout, checkAuth} = authSlice.actions