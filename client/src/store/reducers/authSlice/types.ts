import { UserDataType } from "../../../types"


export interface InitialStateType {
    loggingIn: boolean
    user: UserDataType | {}
}