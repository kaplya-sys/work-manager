import { DataFromStorageType } from './../types'
import * as SecureStore from 'expo-secure-store'
import { LOCAL_STORAGE_VARIABLES } from '../variables'

export const saveDataToStorage = async (token: string, user: string) => {
    try {
        await SecureStore.setItemAsync(LOCAL_STORAGE_VARIABLES.TOKEN, token)
        await SecureStore.setItemAsync(LOCAL_STORAGE_VARIABLES.USER, user)
    } catch(err) {
        console.log(err)
    }
}

export const removeDataFromStorage = async () => {
    try {
        await SecureStore.deleteItemAsync(LOCAL_STORAGE_VARIABLES.TOKEN)
        await SecureStore.deleteItemAsync(LOCAL_STORAGE_VARIABLES.USER)
    } catch(err) {
        console.log(err)
    }
}

export const getDataFromStorage = async (): Promise<DataFromStorageType | undefined> => {
    try {
        const userToken = await SecureStore.getItemAsync(LOCAL_STORAGE_VARIABLES.TOKEN)
        const user = JSON.parse(await SecureStore.getItemAsync(LOCAL_STORAGE_VARIABLES.USER) || '{}')

        return {userToken, user}
    } catch(err) {
        console.log(err)
    }
}

/*type UserType = {
    id: string
    firstName: string
    lastName: string
    isAdmin: boolean
}

export class StorageHelper {
     async setDataToStorage(token: string, user: string) {
        await SecureStore.setItemAsync(LOCAL_STORAGE_VARIABLES.TOKEN, token)
        await SecureStore.setItemAsync(LOCAL_STORAGE_VARIABLES.USER, user)
    }

    async removeDataFromStorage() {
        await SecureStore.deleteItemAsync(LOCAL_STORAGE_VARIABLES.TOKEN)
        await SecureStore.deleteItemAsync(LOCAL_STORAGE_VARIABLES.USER)
    }

    async getDataFromStorage(): Promise<DataFromStorageType> {
        const token = await SecureStore.getItemAsync(LOCAL_STORAGE_VARIABLES.TOKEN)
        const user = JSON.parse(await SecureStore.getItemAsync(LOCAL_STORAGE_VARIABLES.USER) || '{}')

        return {token, user}
    }
}*/