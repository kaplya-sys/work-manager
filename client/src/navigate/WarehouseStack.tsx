import React, { FC } from 'react'
import { createNativeStackNavigator, NativeStackNavigationOptions } from "@react-navigation/native-stack"
import { WarehouseScreen } from '../screens/WarehouseScreen'
import { AddEquipmentScreen } from '../screens/AddEquipmentScreen'
import { WarehouseStackParamList } from './types'

export const WarehouseStack: FC = () => {
    const Stack = createNativeStackNavigator<WarehouseStackParamList>()

    const options: NativeStackNavigationOptions = {
        headerShown: false,
    }

    return (
        <Stack.Navigator screenOptions={options}>
            <Stack.Screen name="Warehouse" component={WarehouseScreen} />
            <Stack.Screen name="AddWarehouse" component={AddEquipmentScreen} />
        </Stack.Navigator>
    )
}