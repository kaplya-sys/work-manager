import React, { FC } from 'react'
import { createNativeStackNavigator, NativeStackNavigationOptions } from "@react-navigation/native-stack"
import { AppointEquipmentScreen } from "../screens/AppointEquipmentScreen"
import { EquipmentScreen } from "../screens/EquipmentScreen"
import { ReassignEquipmentScreen } from '../screens/ReassignEquipmentScreen'
import { RevertEquipmentScreen } from '../screens/RevertEquipmentScreen'
import { EquipmentStackParamList } from './types'

export const EquipmentStack: FC = () => {
    const Stack = createNativeStackNavigator<EquipmentStackParamList>()

    const options: NativeStackNavigationOptions = {
        headerShown: false,
    }

    return (
        <Stack.Navigator screenOptions={options} >
            <Stack.Screen name="Equipment" component={EquipmentScreen} />
            <Stack.Screen name="AppointEquipment" component={AppointEquipmentScreen} />
            <Stack.Screen name="ReassignEquipment" component={ReassignEquipmentScreen} />
            <Stack.Screen name="RevertEquipment" component={RevertEquipmentScreen} />
        </Stack.Navigator>
    )
}