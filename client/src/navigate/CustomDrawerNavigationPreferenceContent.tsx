import { StyleSheet, Switch, Text, View, useColorScheme } from 'react-native'
import React, {FC, useState} from 'react'
import { THEME_VARIABLES } from '../variables'

export const CustomDrawerNavigationPreferenceContent: FC = () => {
	const [darkTheme, setDarkTheme] = useState<boolean>(false)
	const colorScheme = useColorScheme()

	return (
		<View style={styles.preferenceTitleSection}>
			<View style={styles.preferenceSection}>
				<Text style={styles.styleTitle}>Тёмная Тема</Text>
				<Switch
					trackColor={{ false: THEME_VARIABLES.SWITCH_TRACK_COLOR_FALSE, true: THEME_VARIABLES.SWITCH_TRACK_COLOR_TRUE }}
					thumbColor={THEME_VARIABLES.SWITCH_THUMB_COLOR}
					ios_backgroundColor={THEME_VARIABLES.SWITCH_IOS_BACKGROUND_COLOR}
					value={darkTheme}
					onValueChange={() => setDarkTheme(!darkTheme)}
				/>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	preferenceTitleSection: {
		borderTopColor: THEME_VARIABLES.DRAWER_BORDER_COLOR,
		borderTopWidth: 1,
        backgroundColor: THEME_VARIABLES.DRAWER_ITEM_LIST_COLOR
	},
	preferenceSection: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingVertical: 12,
		paddingHorizontal: 20,
	},
	styleTitle: {
		marginTop: 15,
	},
})
