import { TouchableOpacity } from 'react-native'
import React, { FC } from 'react'
import { NavigationContainer } from "@react-navigation/native"
import { createNativeStackNavigator, NativeStackNavigationOptions } from "@react-navigation/native-stack"
import { createDrawerNavigator, DrawerNavigationOptions } from '@react-navigation/drawer'
import { AntDesign, Feather, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons'
import { AuthScreen } from "../screens/AuthScreen"
import { PasswordRecovery } from '../screens/PasswordRecovery'
import { ProfileScreen } from '../screens/ProfileScreen'
import { EmployeesStack } from './EmployeesStack'
import { EquipmentStack } from './EquipmentStack'
import { WarehouseStack } from './WarehouseStack'
import { CustomDrawerNavigation } from './CustomDrawerNavigation'
import { THEME_VARIABLES } from '../variables'
import { DrawerParamList, AuthStackParamList } from './types'
import { useAppSelector } from '../store/app/hook'

export const RootNavigation: FC = () => {
    const { loggingIn } = useAppSelector(state => state.authState)
    const { isAdmin } = useAppSelector(state => state.authState.user)

    const Stack = createNativeStackNavigator<AuthStackParamList>()
    const Drawer = createDrawerNavigator<DrawerParamList>()

    const rootStackNavigatorOptions: NativeStackNavigationOptions = {
        headerStyle: {
            backgroundColor: THEME_VARIABLES.HEADER_COLOR,
        }
    }

    const drawerNavigatorOptions: DrawerNavigationOptions = {
        drawerActiveBackgroundColor: THEME_VARIABLES.DRAWER_ACTIVE_BACKGROUND_COLOR,
        drawerInactiveTintColor: THEME_VARIABLES.DRAWER_INACTIVE_TINT_COLOR,
        drawerLabelStyle: { marginLeft: -25 },
        headerStyle: {
            backgroundColor: THEME_VARIABLES.HEADER_COLOR,
        }
    }

    const singUpScreenOptions: NativeStackNavigationOptions = {
        title: 'Авторизация',
    }

    const passwordRecoveryScreenOptions: NativeStackNavigationOptions = {
        title: 'Восстановление пароля',
    }

    const homeScreenOptions: DrawerNavigationOptions = {
        title: "Главная",
        drawerIcon: ({ color }) => <AntDesign name="home" size={24} color={color} />,
    }

    const warehouseStackOptions: DrawerNavigationOptions = {
        title: "Склад",
        drawerIcon: ({ color }) => <Feather name="package" size={24} color={color} />,
        /*headerRight: () => (
            <TouchableOpacity style={{ marginRight: 17 }} onPress={() => console.log('1')}>
                <MaterialCommunityIcons name="barcode-scan" size={24} color="black" />
            </TouchableOpacity>
        )*/
    }

    const employeesStackOptions: DrawerNavigationOptions = {
        title: "Сотрудники",
        drawerIcon: ({ color }) => <Feather name="users" size={24} color={color} />,
    }

    const profileScreenOptions: DrawerNavigationOptions = {
        title: "Профиль",
        drawerIcon: ({ color }) => <AntDesign name="profile" size={24} color={color} />,
    }

    const equipmentStackOptions: DrawerNavigationOptions = {
        title: "Инвентарь",
        drawerIcon: ({ color }) => <MaterialIcons name="backpack" size={24} color={color} />,
    }

    return (
        <NavigationContainer>
            {!loggingIn ? (
                <Stack.Navigator screenOptions={rootStackNavigatorOptions}>
                    <Stack.Screen name="SingUp" component={AuthScreen} options={singUpScreenOptions} />
                    <Stack.Screen name="PasswordRecovery" component={PasswordRecovery} options={passwordRecoveryScreenOptions} />
                </Stack.Navigator>
            ) : (
                isAdmin ? (
                    <Drawer.Navigator
                        initialRouteName="Home"
                        drawerContent={props => <CustomDrawerNavigation {...props} />}
                        screenOptions={drawerNavigatorOptions}
                    >
                        <Drawer.Screen
                            name="Home"
                            component={WarehouseStack}
                            options={homeScreenOptions}
                        />
                        <Drawer.Screen
                            name="WarehouseStack"
                            component={WarehouseStack}
                            options={warehouseStackOptions}
                        />
                        <Drawer.Screen
                            name="EmployeesStack"
                            component={EmployeesStack}
                            options={employeesStackOptions}
                        />
                        <Drawer.Screen
                            name="Profile"
                            component={ProfileScreen}
                            options={profileScreenOptions}
                        />
                    </Drawer.Navigator>
                ) : (
                    <Drawer.Navigator
                        initialRouteName="Home"
                        drawerContent={props => <CustomDrawerNavigation {...props} />}
                        screenOptions={drawerNavigatorOptions}
                    >
                        <Drawer.Screen
                            name="Home"
                            component={EquipmentStack}
                            options={homeScreenOptions}
                        />
                        <Drawer.Screen
                            name="EquipmentStack"
                            component={EquipmentStack}
                            options={equipmentStackOptions}
                        />
                    </Drawer.Navigator>
                )
            )}
        </NavigationContainer>
    )
}