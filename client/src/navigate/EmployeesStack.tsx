import React, { FC } from 'react'
import { createNativeStackNavigator, NativeStackNavigationOptions } from "@react-navigation/native-stack"
import { EmployeesScreen } from '../screens/EmployeesScreen'
import { AddEmployeesScreen } from '../screens/AddEmployeesScreen'
import { EmployeesStackParamList } from './types'

export const EmployeesStack: FC = () => {
    const Stack = createNativeStackNavigator<EmployeesStackParamList>()

    const options: NativeStackNavigationOptions = {
        headerShown: false,
    }

    return (
        <Stack.Navigator screenOptions={options}>
            <Stack.Screen name="Employees" component={EmployeesScreen} />
            <Stack.Screen name="AddEmployees" component={AddEmployeesScreen} />
        </Stack.Navigator>
    )
}