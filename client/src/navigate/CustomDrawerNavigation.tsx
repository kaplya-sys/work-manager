import React, { FC } from 'react'
import { View, StyleSheet } from 'react-native'
import { DrawerContentComponentProps, DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer'
import { CustomDrawerNavigationTopContent } from './CustomDrawerNavigationTopContent'
import { CustomDrawerNavigationPreferenceContent } from './CustomDrawerNavigationPreferenceContent'
import { CustomDrawerNavigationBottomContent } from './CustomDrawerNavigationBottomContent'
import { THEME_VARIABLES } from '../variables'

export const CustomDrawerNavigation: FC<DrawerContentComponentProps> = props => {

	return (
		<View style={styles.container} >
			<DrawerContentScrollView
				{...props}
				contentContainerStyle={{
					backgroundColor: THEME_VARIABLES.HEADER_COLOR
				}}
			>
				<CustomDrawerNavigationTopContent />

				<View style={styles.itemList}>
					<DrawerItemList {...props} />
				</View>
				<CustomDrawerNavigationPreferenceContent />
			</DrawerContentScrollView>
			<CustomDrawerNavigationBottomContent />
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	itemList: {
		backgroundColor: THEME_VARIABLES.DRAWER_ITEM_LIST_COLOR
	},
})