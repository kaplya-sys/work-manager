import { StyleSheet, View } from 'react-native'
import React, { FC } from 'react'
import { DrawerItem } from '@react-navigation/drawer'
import { Ionicons } from '@expo/vector-icons'
import { THEME_VARIABLES } from '../variables'
import { removeDataFromStorage } from '../helpers/StorageHelper'
import { useAppDispatch } from '../store/app/hook'
import { logout } from '../store/reducers/authSlice'

export const CustomDrawerNavigationBottomContent: FC = () => {
    const dispatch = useAppDispatch()
    
    return (
        <View style={styles.bottomDrawerSection}>
            <DrawerItem
                icon={() => (
                    <Ionicons name="ios-exit-outline" size={24} color="black" />
                )}
                label="Выход"
                inactiveTintColor={THEME_VARIABLES.DRAWER_ITEM_INACTIVE_TIN_COLOR}
                onPress={() => {
                    removeDataFromStorage()
                    dispatch(logout())
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    bottomDrawerSection: {
        marginHorizontal: 5,
		borderTopColor: THEME_VARIABLES.DRAWER_BORDER_COLOR,
		borderTopWidth: 1,
	},
})