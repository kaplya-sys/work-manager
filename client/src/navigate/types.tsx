import { NavigatorScreenParams } from '@react-navigation/native'

type IRouteProps = {
    equipment: string
    serial: string
}

export type AuthStackParamList = {
    SingUp: undefined
    PasswordRecovery: undefined
}

export type EmployeesStackParamList = {
    Employees: undefined
    AddEmployees: undefined
}

export type EquipmentStackParamList = {
    Equipment: undefined
    AppointEquipment: IRouteProps
    ReassignEquipment: IRouteProps
    RevertEquipment: IRouteProps
}

export type WarehouseStackParamList = {
    Warehouse: undefined
    AddWarehouse: undefined
}

export type DrawerParamList = {
    Home: undefined
    Profile: undefined
    WarehouseStack: NavigatorScreenParams<WarehouseStackParamList>
    EmployeesStack: NavigatorScreenParams<EmployeesStackParamList>
    EquipmentStack: NavigatorScreenParams<EquipmentStackParamList>
}

declare global {
    namespace ReactNavigation {
      interface RootParamList extends AuthStackParamList, DrawerParamList {}
    }
  }