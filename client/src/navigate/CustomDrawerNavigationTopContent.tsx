import { Image, StyleSheet, Text, View } from 'react-native'
import React, { FC } from 'react'
import { useAppSelector } from '../store/app/hook'

export const CustomDrawerNavigationTopContent: FC = () => {
	const {firstName, lastName} = useAppSelector(state => state.authState.user)

	return (
		<View style={styles.container}>
			<Image
				source={require('../../assets/default-avatar.png')}
				style={{ width: 60, height: 60 }}
			/>
			<Text style={styles.title}>{`${firstName} ${lastName}`}</Text>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 180,
	},
	title: {
		fontSize: 16,
		marginTop: 5,
		fontFamily: 'roboto-regular-font',
	},
})