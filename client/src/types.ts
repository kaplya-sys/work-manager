export interface UserDataType {
    id: string
    firstName: string
    lastName: string
    avatar: string
    isAdmin: boolean
}

export interface AuthUserResponseDataType {
    userToken: string
    user: UserDataType
}

export interface AuthDataType {
    login: string
    password: string
}

export interface CreateUserDataType {
    firstName: string
    lastName: string
    patronymic: string
    company: string
    login: string
    password: string
    isAdmin: boolean
}

export interface DataFromStorageType {
    userToken: string | null
    user: UserDataType
}