import axios, {AxiosRequestConfig} from "axios"
import * as SecureStore from 'expo-secure-store'
import { LOCAL_STORAGE_VARIABLES } from '../variables'

export const http = axios.create({
    baseURL: 'http://192.168.110.74:5000/api'
})

http.interceptors.request.use(async(config: AxiosRequestConfig): Promise<AxiosRequestConfig> => {
    const token: string | null = await SecureStore.getItemAsync(LOCAL_STORAGE_VARIABLES.TOKEN)

    if (token) {
        config.headers!.Authorization = token
    }
    return config
})