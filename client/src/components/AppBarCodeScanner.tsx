import React, { useState, useEffect, FC } from 'react'
import { Alert, Dimensions, Text, View, StyleSheet, Button } from 'react-native'
import { BarCodeBounds, BarCodePoint, BarCodeScanner, BarCodeScannerResult } from 'expo-barcode-scanner'
import BarcodeMask from 'react-native-barcode-mask'

const finderWidth = 280
const finderHeight = 630
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const viewMinX = (width - finderWidth) / 2
const viewMinY = (height - finderHeight) / 2

export const AppBarCodeScanner: FC = () => {
    const [hasPermission, setHasPermission] = useState<boolean | null>(null)
    const [scanned, setScanned] = useState<boolean>(false)
    const [serialNumbers, setSerialNumbers] = useState<string[]>([])

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync()
            setHasPermission(status === 'granted')
        })()
    }, [])

    const handleBarCodeScanned = (scanningResult: BarCodeScannerResult) => {
        const {type, data, bounds: {origin} = {}} = scanningResult

        if (!scanned) {
            const {x, y} = origin

            if (x >= viewMinX && y >= viewMinY && x <= (viewMinX + finderWidth / 2) && y <= (viewMinY + finderHeight / 2)) {
                setSerialNumbers(prev => [...prev, data])
                setScanned(true)
                Alert.alert(`${data}`)
            }
        }
    }

    if (hasPermission === null) {
        return <Text>Предоставьте доступ на использование камеры</Text>
    }
    if (hasPermission === false) {
        return <Text>Не были даны разрешения на использование камеры</Text>
    }

    return (
        <View style={styles.container}>
            <BarCodeScanner
                barCodeTypes={[]}
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={StyleSheet.absoluteFillObject}
            />
            <BarcodeMask edgeColor="#62B1F6" showAnimatedLine />
            {
            scanned && <Button
            title={'Просканировать ещё...'}
            onPress={() => setScanned(false)}
            />
            }
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
})