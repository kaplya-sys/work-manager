import React from "react"
import { ActivityIndicator, StyleSheet, View } from "react-native"
import { THEME_VARIABLES } from "../variables"

export const Loader = () => (
  <View style={[styles.container, styles.horizontal]}>
    <ActivityIndicator size="large" color={THEME_VARIABLES.HEADER_COLOR} />
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
  }
})