import { Modal, Pressable, StyleSheet, Text, View } from 'react-native'
import React, { FC } from 'react'
import { useNavigation } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { EquipmentStackParamList } from '../navigate/types'

type Props = {
    equipmentData: {
        serial: string
        equipment: string
    }
    hideModal: () => void
    visible: boolean
}

type EquipmentModalWindowNavigationProp = NativeStackNavigationProp<EquipmentStackParamList>

export const EquipmentModalWindow: FC<Props> = ({ equipmentData, hideModal, visible }) => {
    const navigation = useNavigation<EquipmentModalWindowNavigationProp>()
    const { serial, equipment } = equipmentData

    return (
        <View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={visible}
            >
                <View style={styles.wrapperView}>
                    <View style={styles.modalView}>
                        <Pressable
                            onPress={() => {
                                console.log("y")
                                navigation.navigate('AppointEquipment', { equipment, serial })
                                hideModal()
                            }}
                        >
                            <Text
                                style={styles.modalText}>
                                Списать
                            </Text>

                        </Pressable>
                        <Pressable
                            onPress={() => {
                                navigation.navigate('RevertEquipment', { equipment, serial })
                                hideModal()
                            }}
                        >
                            <Text
                                style={styles.modalText}>
                                Вернуть
                            </Text>
                        </Pressable>
                        <Pressable
                            onPress={() => {
                                navigation.navigate('ReassignEquipment', { equipment, serial })
                                hideModal()
                            }}
                        >
                            <Text
                                style={styles.modalText}>
                                Переназначить
                            </Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapperView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 10,
        padding: 25,
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: '60%'
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'left',
        fontFamily: 'roboto-bold-font'
    }
})