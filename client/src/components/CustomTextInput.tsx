import { StyleSheet, TextInput, TextInputProps, View } from 'react-native'
import React, { FC } from 'react'

export const CustomTextInput: FC<TextInputProps> = (props) => {
	return (
		<View style={styles.container}>
			<TextInput
				{...props}
				style={{ ...styles.input, ...props.style as object}}
				editable
				autoCapitalize="none"
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#FFF',
		width: '100%',
		borderBottomWidth: 0.2,
		borderColor: "#E8E8E8",
		borderRadius: 5,
		paddingHorizontal: 10,
		marginVertical: 5
	},
	input: {
		padding: 3
	}
})