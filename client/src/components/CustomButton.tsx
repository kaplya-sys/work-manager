import { Pressable, StyleSheet, Text, TouchableOpacity } from 'react-native'
import React, { FC } from 'react'
import { THEME_VARIABLES } from '../variables'

type Props = {
    onPress: () => void
    text: string
    type: string
}

export const CustomButton: FC<Props> = ({ onPress, text, type }) => {
    const containerKey = `container_${type}` as keyof typeof styles
    const textKey = `text_${type}` as keyof typeof styles

    return (
        <TouchableOpacity
            onPress={onPress}
            style={[styles.container, styles[containerKey]]}
        >
            <Text
                style={[styles.text, styles[textKey]]}
            >
                {text}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        padding: 15,
        marginVertical: 5,
        alignItems: 'center',
        borderRadius: 5
    },
    text: {
        fontFamily: 'roboto-bold-font',
        color: '#FFF',

    },
    container_filled: {
        backgroundColor: THEME_VARIABLES.HEADER_COLOR,
    },
    container_lowercase: {

    },
    text_filled: {

    },
    text_lowercase: {
        color: '#708090'
    }
})