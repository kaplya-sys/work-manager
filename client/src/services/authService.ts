import { CreateUserDataType } from './../types';
import { AxiosResponse } from 'axios'
import { http } from '../http'
import { AuthDataType, AuthUserResponseDataType } from '../types'

export const authorization = async (data: AuthDataType): Promise<AxiosResponse<AuthUserResponseDataType>> => {
    return await http.post<AuthUserResponseDataType>('/auth/login', data)
}

export const registration = async (data: CreateUserDataType): Promise<void> => {
    return await http.post('/auth/create', data)
}